
// The ItemComponent matches one of two different routes
// depending on the full pathname
const SliderComponent = Slider.default;
const ReactBootstrapComponent = ReactBootstrap;
//console.log(ReactBootstrapComponent)
const ProductComponent = ({ items, setItems }) => {
	React.useEffect(() => {
		
	}, [])
	
return(
  <Switch>
    <Route exact path={['/products']} render={() => ( <Products items={items} setItems={setItems} /> )}/>
	<Route path={["/products/:id"]} render={({ match }) => {
		const itemId = match.params.id
		const filteredItem = items.find(x => x.id == itemId)
		//console.log(filteredItem)
        return <Product itemId={itemId} filteredItem={filteredItem} />;
    }} />
  </Switch>
)}



// The Items iterates over all of the items and creates
// a link to their profile page.
const Products = ({ items, setItems }) => {
	React.useEffect(() => {
		setNewItems(items)
		getItems()
	}, [])
	
	const [newItems, setNewItems] = React.useState([]);
	const [searchDetails, setSearchDetails] = React.useState('Haetaan viimeisimmät tuotteet... ');
	
	const debounce = (fn, delay = 400) => {
		let timeoutId;
		return (...args) => {
			if (timeoutId) {
				clearTimeout(timeoutId);
			}
			timeoutId = setTimeout(() => {
				fn.apply(null, args)
			}, delay);
		};
	};

	const searchItems = debounce(async (searchParam) => {
		if (searchParam.length > 1) {
			fetch('http://localhost/cfd/factorapi/search/'+searchParam).then(function (response) {
				return response.json();
			}).then(function (json) {
				setNewItems(json)
				setSearchDetails(`Haetaan tuotteet hakusanalla "${searchParam}", ${json.length} tulosta`)
			}).catch(function (err) {
				console.warn('Something went wrong.', err);
				alert(err)
			})
		} else if (searchParam.length < 1) {
			getItems()
		}
	});
	
	const getItems = () => {
		fetch('http://localhost/cfd/factorapi/factors').then(function (response) {
			return response.json();
		}).then(function (json) {
			setNewItems(json)
			setSearchDetails(`Getting the latest products, ${json.length} result(s)`)
		}).catch(function (err) {
			console.warn('Something went wrong.', err);
			alert(err)
		})
	};

  return (
  <div>
  <h3><u>Factors and products2</u></h3>
	<label style={{marginBottom: '0px'}}><b>Search products:</b></label><br/>
	<i>{searchDetails}</i><br/>
	<input className="form-control input-sm" style={{marginTop: '3px', maxWidth: '800px', marginBottom: '30px'}} type="text" onChange={e =>searchItems(e.target.value)} />
	<label><b>Found products:</b></label>
    {/*<ul style={{paddingLeft: '14px'}}>
      {
        newItems.map(p => (
          <li key={p.id}>
            <Link to={`/products/${p.id}`}>{p.brand}</Link>
          </li>
        ))
      }
    </ul>*/}
    <div className="row" style={{marginLeft: '0px',border: '1px solid lightGray', maxWidth: '1200px', boxShadow: '1px 1px 10px #808080'}}>
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Category</b></div>
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Group</b></div>
      <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Brand</b></div>
      <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Model</b></div>
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Version</b></div>
	  <div className="col" data-toggle="tooltip" title="Total emissions / total emission deviation" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Total/dev</b></div>
	  <div className="col" data-toggle="tooltip" title="Operation emissions / operation emission deviation" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Oper/dev</b></div>
	  <div className="col" data-toggle="tooltip" title="Production emissions / production emission deviation" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Prod/dev</b></div>
	  <div className="col" data-toggle="tooltip" title="Disposal emissions / disposal emission deviation" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Dispo/dev</b></div>
	  <div className="col" style={{padding: '10px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}><b>Added</b></div>
      <div className="col" style={{padding: '10px', backgroundColor: 'white', fontSize: '14px'}}><b>Details</b></div>
    </div>
	{
	newItems.map(p => {
		return(
		<div key={p.id} className="row" style={{marginLeft: '0px', border: '1px solid lightGray', maxWidth: '1200px', boxShadow: '1px 1px 10px #808080'}}>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.category}</div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.group}</div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', maxWidth: '120px', fontSize: '14px'}}>{p.brand}</div>
		  <div className="col" style={{overflow: 'auto', padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', maxWidth: '240px', fontSize: '14px'}}>{p.model}</div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.versionInfo}</div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
		  { p.category == 'vehicle' ? <div>{p.totalCO2}<b>/</b>{p.totalCO2dev} {p.emissionUnit}</div> : <div>{p.totalCO2/1000}<b>/</b>{p.totalCO2dev/1000} {p.emissionUnit}</div> }
		  </div>
		  <div className="col" style={{padding: '5px',borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
		  { p.category == 'vehicle' ? <div>{p.operationCO2}<b>/</b>{p.operationCO2dev} {p.emissionUnit}</div> : <div>{p.operationCO2/1000}<b>/</b>{p.operationCO2dev/1000} {p.emissionUnit}</div> }
		  </div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
		  { p.category == 'vehicle' ? <div>{p.productionCO2}<b>/</b>{p.productionCO2dev} {p.emissionUnit}</div> : <div>{p.productionCO2/1000}<b>/</b>{p.productionCO2dev/1000} {p.emissionUnit}</div> }
		  </div>
		  <div className="col" style={{padding: '5px',borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '13px'}}>
		  { p.category == 'vehicle' ? <div>{p.disposalCO2}<b>/</b>{p.disposalCO2dev} {p.emissionUnit}</div> : <div>{p.disposalCO2/1000}<b>/</b>{p.disposalCO2dev/1000} {p.emissionUnit}</div> }
		  </div>
		  <div className="col" style={{padding: '5px', borderRight: '1px solid lightGray', backgroundColor: 'white', fontSize: '14px'}}>{p.date}</div>
		  <div className="col" style={{padding: '5px', backgroundColor: 'white', fontSize: '14px'}}><Link to={`/products/${p.id}`}>
		  <button type="submit" className="btn btn-info" style={{paddingTop: '2px', paddingBottom: '2px', paddingLeft: '15px', paddingRight: '15px', fontSize: '14px'}}>Show</button>
		  </Link></div>
    </div>)})
	}	
  </div>
)}

// The Item looks up the item using the id parsed from
// the URL's pathname. If no item is found with the given
// id, then a "item not found" message is displayed.
const Product = ({itemId, filteredItem}) => {
	React.useEffect(() => {
		document.documentElement.scrollTop = 0
		if (filteredItem) {
			const foundItem = filteredItem
			foundItem.images = []
			setItem(foundItem)
			if (foundItem.extraData.includes('icecatProduct=')) {
				const icecatId = foundItem.extraData.split('icecatProduct=')[1].split(';')[0];
				fetchIcecatData(foundItem.brand, icecatId);
			}
		} else {
			fetch('http://localhost/cfd/factorapi/factors/'+itemId).then(function (response) {
				return response.json();
			}).then(function (json) {
				if (json.extraData.includes('icecatProduct=')) {
					const icecatId = json.extraData.split('icecatProduct=')[1].split(';')[0];
					fetchIcecatData(json.brand, icecatId);
				} else {
					alert('No IcecatId!');
				}
			}).catch(function (err) {
				console.warn('Something went wrong.', err);
			})
		}
	}, [])
	
	const [isLoading, setIsLoading] = React.useState(true);
	
	const [item, setItem] = React.useState({
		title: '',
		brand: '',
		model: '',		
		images: []
	});
	
	const fetchIcecatData = async (brand, icecatId) => {
		try {
			const res = await fetch(`https://live.icecat.biz/api/?UserName=matti@greencarbon.fi&Language=en&Brand=${brand}&ProductCode=${icecatId}`);
			if (!res.ok) {
				setItem(null)
				throw new Error("Fetch was not OK");
			}
			const json = await res.json();			
			const product = {
				title: json.data.GeneralInfo.Title,
				brand: json.data.GeneralInfo.Brand,
				model: json.data.GeneralInfo.ProductName,		
				images: json.data.Gallery
			}
			setItem(product);
			setIsLoading(false);
	  } catch (error) {
		setItem(null)
		alert(`Error fetching or parsing data: ${error}`);
	  }
	};
  
  if (!item) {
    return <div>Sorry, but the product was not found on the Icecat database.</div>
  }
  return (
  <div>
	<ReactBootstrapComponent.Breadcrumb>
	<ReactBootstrapComponent.Breadcrumb.Item href="/cfd/#/">Home</ReactBootstrapComponent.Breadcrumb.Item>
	<ReactBootstrapComponent.Breadcrumb.Item href="/cfd/#/items/">Factors</ReactBootstrapComponent.Breadcrumb.Item>
	<ReactBootstrapComponent.Breadcrumb.Item href={"/cfd/#/items/"+itemId}>Factor</ReactBootstrapComponent.Breadcrumb.Item>
	<ReactBootstrapComponent.Breadcrumb.Item active>Product</ReactBootstrapComponent.Breadcrumb.Item>
	</ReactBootstrapComponent.Breadcrumb>
    <div>{/* item.Dictionary ? item.Dictionary.ean_code : '' */}
      <Link to={'/items/'+itemId}><button type="submit" className="btn btn-info" style={{paddingTop: '2px', paddingBottom: '2px', paddingLeft: '15px', paddingRight: '15px', fontSize: '14px'}}>Back</button></Link>
    </div>
	
	{ isLoading ? 
	<div style={{marginTop: '10px', border: '1px solid lightGray', padding: '10px', borderRadius: '5px', display: 'flex', justifyContent: 'center'}}>                                  
	<div className="spinner-border"></div></div>
	:
	<div>
		<div className="row" style={{marginTop: '30px'}}>
		<div className="col" style={{paddingTop: '5px', borderLeft: '1px solid lightGray', borderRight: '1px solid lightGray', borderTop: '1px solid lightGray', backgroundColor: '#f2f2f2', boxShadow: '1px 1px 4px #808080', marginBottom: '5px'}}>
			<h5>{item.title}&nbsp;</h5>
		</div>
	</div>
	<div className="row" style={{marginTop: '0px'}}>
    <div className="col" style={{borderBottom: '1px solid lightGray', borderLeft: '1px solid lightGray', borderRight: '1px solid lightGray', borderTop: '1px solid lightGray', height: '800px'}}>
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Brand:</b> Brand</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Product code:</b> 321</div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}><b>Product family</b> Family</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}><b>EAN/UPC code</b>: EAN/UPC</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Product name:</b> Product name</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Category:</b> Category</div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}><b>&nbsp;</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}><b>&nbsp;</b></div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Data source URL</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{display: 'grid', padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}><div><a href='123' style={{fontSize: '12px', paddingTop: '2px', paddingBottom: '2px', marginBottom: '2px'}} className="btn btn-info" role="button">Link to Data source</a></div></div>
		</div>
		{/*# EMISSION START #*/}
		<div className="row" style={{borderTop: '1px solid lightGray', marginTop: '30px'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Total CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Total CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>1&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>1</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Production CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Production CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>1&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>1</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Operation CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Operation CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>1&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>1</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Disposal CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Disposal CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>1&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>1</div>
		</div>
		{/*# EMISSION END #*/}
		<div className="row" style={{borderTop: '1px solid lightGray', marginTop: '40px'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>EAN</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>UPC</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>1&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>1</div>
		</div>
		<div className="row" style={{borderTop: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Extra data</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Source</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>1&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>1</div>
		</div>
		<div className="row" style={{borderTop: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Version Info</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>External ID</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>1&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>1</div>
		</div>
		
    </div>
    <div className="col" style={{borderTop: '1px solid lightGray', borderRight: '1px solid lightGray', borderBottom: '1px solid lightGray', height: '800px'}}>
	{/*<div style={{border: '1px solid black', margin: 'auto', height: '250px', width: '250px', backgroundImage: 'url(https://greencarbon.fi/wp-content/uploads/2020/12/placeholder.png)', backgroundPosition: 'center', backgroundRepeat: 'no-repeat'}}>
	</div>*/}

	{/*
	const settings = {
		dots: true,
		infinite: true,
		speed: 500,
		slidesToShow: 1,
		slidesToScroll: 1
    };
	
	const imageUrls = [
		'http://images.icecat.biz/img/gallery/19167278_6571.jpg',
        'http://images.icecat.biz/img/norm/high/19167278-7605.jpg',
        'http://images.icecat.biz/img/gallery/19167278_1529.jpg',
        'http://images.icecat.biz/img/gallery/19167278_7975.jpg'
	]
    <div style={{maxWidth: '400px', maxHeight: '400px'}}>
      Slider:
      <SliderComponent {...settings}>
		{imageUrls.map(url => {
		return(
		<div key={url} >
         <img src={url} style={{maxWidth: '400px', maxHeight: '400px'}} />
		</div>)})}
      </SliderComponent>
    </div>*/}
	
	<div style={{marginLeft: 'auto', marginRight: 'auto', minHeight: '300px', minWidth: '400px', maxWidth: '400px', maxHeight: '300px'}}>
		<ReactBootstrap.Carousel>
		{item.images.map((img, idx) => {
			return(
		<ReactBootstrap.Carousel.Item interval={3000} key={idx}>
			<img style={{minHeight: '300px', minWidth: '400px', maxWidth: '400px', maxHeight: '300px'}}
			className="d-block w-100"
			src={img.Pic500x500}
			alt="First slide"
			/>
		<ReactBootstrap.Carousel.Caption>
			<h4 style={{}}>{item.model}</h4>
			<a href={img.Pic} style={{fontSize: '12px', paddingTop: '2px', paddingBottom: '2px', marginBottom: '2px'}} className="btn btn-info" role="button">View full image</a>
		</ReactBootstrap.Carousel.Caption>
		</ReactBootstrap.Carousel.Item>
		)})}
		</ReactBootstrap.Carousel>
	</div>
	
	<div className="row" style={{borderTop: '1px solid lightGray', marginTop: '30px'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Total CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Total CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>123&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>123</div>
		</div>
		
		<div className="row" style={{}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px'}}><b>Production CO2</b></div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderLeft: '1px solid lightGray'}}><b>Production CO2 deviation</b></div>
		</div>
		<div className="row" style={{borderBottom: '1px solid lightGray'}}>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray'}}>321&nbsp;</div>
			<div className="col" style={{padding: '7px', paddingBottom: '2px',fontSize: '14px', borderTop: '1px solid lightGray', borderLeft: '1px solid lightGray'}}>321</div>
	</div>
	123
    </div>
  </div>
</div> }

  </div>
  )
}