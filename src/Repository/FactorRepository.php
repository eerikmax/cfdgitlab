<?php

namespace App\Repository;

use App\Entity\Factor;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Factor|null find($id, $lockMode = null, $lockVersion = null)
 * @method Factor|null findOneBy(array $criteria, array $orderBy = null)
 * @method Factor[]    findAll()
 * @method Factor[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FactorRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Factor::class);
    }
	
	/*
	public function findFactorsUsingRange($searchFrom, $useRange) {
		$rangeStart = 1292670089; //2010
		$rangeEnd = 1450436489; //2015

		$data = $this->createQueryBuilder('p');
		$data = $data->where('p.date > :rangeStart')
			->andWhere('p.date < :rangeEnd')
			->setParameter('rangeStart', $rangeStart)
			->setParameter('rangeEnd', $rangeEnd);
		
		$data = $data
			->orderBy('p.id', 'ASC')
			->setMaxResults(100)
			->getQuery()
			->getResult();
		
		return $data;
    }*/

    public function findByBrandOrModel($value, $searchFrom) {
		
		$data = $this->createQueryBuilder('p');
		if ($value != '' && $searchFrom == 'brand') {
			$data = $data->where('p.brand LIKE :val');
		} else if ($value != '' && $searchFrom == 'model') {
			$data = $data->where('p.model LIKE :val');
		}
		$data = $data
			->setParameter('val', '%'.$value.'%')
			->orderBy('p.id', 'ASC')
			->setMaxResults(100)
			->getQuery()
			->getResult();
		
		return $data;
    }
    // /**
    //  * @return Factor[] Returns an array of Factor objects
    //  */
    public function findByExampleField($value)
    {
		$parts = explode(' ', $value);
		$firstParam = $parts[0];
		//return $firstParam;

        $data = $this->createQueryBuilder('p')
			->where('p.brand LIKE :val')
			->orWhere('p.model LIKE :val')			
			->setParameter('val', '%'.$firstParam.'%')
			->orderBy('p.id', 'ASC')
			->setMaxResults(100)
			->getQuery()
			->getResult();
		
		if (isset($parts[1])) {
			$filteredItems = array();
			$secondParam = $parts[1];
			if (isset($parts[2])) {
				$thirdParam = $secondParam." ".$parts[2];
				foreach ($data as $i => $value) {
					if ((strpos(strtolower($data[$i]->getModel()), strtolower($thirdParam)) !== false)) {
						$filteredItems[] = $data[$i];
					}
				}
			} else {
				foreach ($data as $i => $value) {
					if ((strpos(strtolower($data[$i]->getModel()), strtolower($secondParam)) !== false)
						|| (strpos(strtolower($data[$i]->getBrand()), strtolower($secondParam)) !== false)) {
						$filteredItems[] = $data[$i];
					}
				}
			}
			$data = $filteredItems;
		}
		
		return $data;
		
    }

    /*
    public function findOneBySomeField($value): ?Post
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}