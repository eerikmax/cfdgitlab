<?php
namespace App\Entity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
* @ORM\Entity
* @ORM\Table(name="factor")
* @ORM\HasLifecycleCallbacks()
*/
class Factor implements \JsonSerializable {
	/**
	* @ORM\Column(type="integer")
	* @ORM\Id
	* @ORM\GeneratedValue(strategy="AUTO")
	*/
	private $id;
	
	/**
	* @ORM\Column(type="string", length=100)
	*
	*/
	private $category;
	
	/**
	* @ORM\Column(type="string", length=100)
	*
	*/
	private $group;

	/**
	* @ORM\Column(type="string", length=100)
	*
	*/
	private $brand;

	/**
	* @ORM\Column(type="string", length=100)
	*
	*/
	private $model;
	
	/**
	* @ORM\Column(type="string", length=1000)
	*
	*/
	private $externalUrl;

	/**
	* @Assert\Length(max=11)
	* @ORM\Column(type="integer")
	*/
	private $totalCO2;
	
	/**
	* @Assert\Length(max=11)
	* @ORM\Column(type="integer")
	*/
	private $totalCO2dev;
	
	/**
	* @Assert\Length(max=11)
	* @ORM\Column(type="integer")
	*/
	private $productionCO2;
	
	/**
	* @Assert\Length(max=11)
	* @ORM\Column(type="integer")
	*/
	private $productionCO2dev;
	
	/**
	* @Assert\Length(max=11)
	* @ORM\Column(type="integer")
	*/
	private $operationCO2;
	
	/**
	* @Assert\Length(max=11)
	* @ORM\Column(type="integer")
	*/
	private $operationCO2dev;
	
	/**
	* @Assert\Length(max=11)
	* @ORM\Column(type="integer")
	*/
	private $disposalCO2;
	
	/**
	* @Assert\Length(max=11)
	* @ORM\Column(type="integer")
	*/
	private $disposalCO2dev;
	
	/**
	* @ORM\Column(type="string", length=1000)
	*
	*/
	private $dataSourceUrl;
	
	/**
	* @Assert\Length(max=11)
	* @ORM\Column(type="integer")
	*/
	private $date;
	
	/**
	* @ORM\Column(type="string", length=1000)
	*
	*/
	private $ean;
	
	/**
	* @ORM\Column(type="string", length=1000)
	*
	*/
	private $upc;

	/**
	* @ORM\Column(type="string", length=100)
	*
	*/
	private $emissionUnit;
	
	/**
	* @ORM\Column(type="string", length=100)
	*
	*/
	private $extraData;
	
	/**
	* @ORM\Column(type="string", length=100)
	*
	*/
	private $source;
	
	/**
	* @ORM\Column(type="string", length=100)
	*
	*/
	private $versionInfo;
	
	/**
	* @ORM\Column(type="string", length=100)
	*
	*/
	private $externalId;
	
	/**
	* @ORM\Column(type="string", length=100)
	*
	*/
	private $stars;


	/**
	* @return mixed
	*/
	public function getId()
	{
		return $this->id;
	}
	/**
	* @param mixed $id
	*/
	public function setId($id)
	{
		$this->id = $id;
	}
	
	/**
	* @return mixed
	*/
	public function getCategory()
	{
		return $this->category;
	}
	/**
	* @param mixed $category
	*/
	public function setCategory($category)
	{
		$this->category = $category;
	}
	
	/**
	* @return mixed
	*/
	public function getGroup()
	{
		return $this->group;
	}
	/**
	* @param mixed $group
	*/
	public function setGroup($group)
	{
		$this->group = $group;
	}
	
	/**
	* @return mixed
	*/
	public function getBrand()
	{
		return $this->brand;
	}
	/**
	* @param mixed $brand
	*/
	public function setBrand($brand)
	{
		$this->brand = $brand;
	}
	/**
	* @return mixed
	*/
	public function getModel()
	{
		return $this->model;
	}
	/**
	* @param mixed $model
	*/
	public function setModel($model)
	{
		$this->model = $model;
	}
	
	/**
	* @return mixed
	*/
	public function getExternalUrl()
	{
		return $this->externalUrl;
	}
  
	/**
	* @param mixed $externalUrl
	*/
	public function setExternalUrl($externalUrl)
	{
		$this->externalUrl = $externalUrl;
	}

	/**
	* @return mixed
	*/
	public function getTotalCO2()
	{
		return $this->totalCO2;
	}
	/**
	* @param mixed $totalCO2
	*/
	public function setTotalCO2($totalCO2)
	{
		$this->totalCO2 = $totalCO2;
	}
	
	/**
	* @return mixed
	*/
	public function getTotalCO2dev()
	{
		return $this->totalCO2dev;
	}
	/**
	* @param mixed $totalCO2dev
	*/
	public function setTotalCO2dev($totalCO2dev)
	{
		$this->totalCO2dev = $totalCO2dev;
	}
	
	/**
	* @return mixed
	*/
	public function getProductionCO2()
	{
		return $this->productionCO2;
	}
	/**
	* @param mixed $productionCO2
	*/
	public function setProductionCO2($productionCO2)
	{
		$this->productionCO2 = $productionCO2;
	}
	
	/**
	* @return mixed
	*/
	public function getProductionCO2dev()
	{
		return $this->productionCO2dev;
	}
	/**
	* @param mixed $productionCO2dev
	*/
	public function setProductionCO2dev($productionCO2dev)
	{
		$this->productionCO2dev = $productionCO2dev;
	}
	
	/**
	* @return mixed
	*/
	public function getOperationCO2()
	{
		return $this->operationCO2;
	}
	/**
	* @param mixed $operationCO2
	*/
	public function setOperationCO2($operationCO2)
	{
		$this->operationCO2 = $operationCO2;
	}
	
	/**
	* @return mixed
	*/
	public function getOperationCO2dev()
	{
		return $this->operationCO2dev;
	}
	/**
	* @param mixed $operationCO2dev
	*/
	public function setOperationCO2dev($operationCO2dev)
	{
		$this->operationCO2dev = $operationCO2dev;
	}
	
	/**
	* @return mixed
	*/
	public function getDisposalCO2()
	{
		return $this->disposalCO2;
	}
	/**
	* @param mixed $disposalCO2
	*/
	public function setDisposalCO2($disposalCO2)
	{
		$this->disposalCO2 = $disposalCO2;
	}
	
	/**
	* @return mixed
	*/
	public function getDisposalCO2dev()
	{
		return $this->disposalCO2dev;
	}
	/**
	* @param mixed $disposalCO2dev
	*/
	public function setDisposalCO2dev($disposalCO2dev)
	{
		$this->disposalCO2dev = $disposalCO2dev;
	}
	
	/**
	* @return mixed
	*/
	public function getDataSourceUrl()
	{
		return $this->dataSourceUrl;
	}
	/**
	* @param mixed $dataSourceUrl
	*/
	public function setDataSourceUrl($dataSourceUrl)
	{
		$this->dataSourceUrl = $dataSourceUrl;
	}
	
	/**
	* @return mixed
	*/
	public function getDate()
	{
		return $this->date;
	}
  
	/**
	* @param mixed $date
	*/
	public function setDate($date)
	{
		$this->date = $date;
	}
	
	/**
	* @return mixed
	*/
	public function getEan()
	{
		return $this->ean;
	}
  
	/**
	* @param mixed $ean
	*/
	public function setEan($ean)
	{
		$this->ean = $ean;
	}
	
	/**
	* @return mixed
	*/
	public function getUpc()
	{
		return $this->upc;
	}
  
	/**
	* @param mixed $upc
	*/
	public function setUpc($upc)
	{
		$this->upc = $upc;
	}

	/**
	* @return mixed
	*/
	public function getEmissionUnit()
	{
		return $this->emissionUnit;
	}
  
	/**
	* @param mixed $emissionUnit
	*/
	public function setEmissionUnit($emissionUnit)
	{
		$this->emissionUnit = $emissionUnit;
	}
	
	/**
	* @return mixed
	*/
	public function getExtraData()
	{
		return $this->extraData;
	}
  
	/**
	* @param mixed $extraData
	*/
	public function setExtraData($extraData)
	{
		$this->extraData = $extraData;
	}
	
	/**
	* @return mixed
	*/
	public function getSource()
	{
		return $this->source;
	}
  
	/**
	* @param mixed $extraData
	*/
	public function setSource($source)
	{
		$this->source = $source;
	}
	
	/**
	* @return mixed
	*/
	public function getVersionInfo()
	{
		return $this->versionInfo;
	}
  
	/**
	* @param mixed $extraData
	*/
	public function setVersionInfo($versionInfo)
	{
		$this->versionInfo = $versionInfo;
	}
	
	/**
	* @return mixed
	*/
	public function getExternalId()
	{
		return $this->externalId;
	}
  
	/**
	* @param mixed $externalId
	*/
	public function setExternalId($externalId)
	{
		$this->externalId = $externalId;
	}
	
	/**
	* @return mixed
	*/
	public function getStars()
	{
		return $this->stars;
	}
  
	/**
	* @param mixed $stars
	*/
	public function setStars($stars)
	{
		$this->stars = $stars;
	}

	/**
	* @throws \Exception
	* @ORM\PrePersist()
	*/
	//public function beforeSave(){
	// $this->create_date = new \DateTime('now', new \DateTimeZone('Africa/Casablanca'));
	//}

	/**
	* Specify data which should be serialized to JSON
	* @link https://php.net/manual/en/jsonserializable.jsonserialize.php
	* @return mixed data which can be serialized by <b>json_encode</b>,
	* which is a value of any type other than a resource.
	* @since 5.4.0
	*/
	public function jsonSerialize()
	{//id 	category 	group 	brand 	model 	external_url 	total_co2 	total_co2dev 	production_co2 	production_co2dev 	operation_co2 	operation_co2dev 	disposal_co2 	disposal_co2dev 	data_source_url 	date 	ean 	upc 	emission_unit 	extra_data 	source 	version_info 	external_id 	stars
		return [
			"id" => $this->getId(),
			"category" => $this->getCategory(),
			"group" => $this->getGroup(),
			"brand" => $this->getBrand(),
			"model" => $this->getModel(),
			"externalUrl" => $this->getExternalUrl(),
			"totalCO2" => $this->getTotalCO2(),
			"totalCO2dev" => $this->getTotalCO2dev(),
			"productionCO2" => $this->getProductionCO2(),
			"productionCO2dev" => $this->getProductionCO2dev(),
			"operationCO2" => $this->getOperationCO2(),
			"operationCO2dev" => $this->getOperationCO2dev(),
			"disposalCO2" => $this->getDisposalCO2(),
			"disposalCO2dev" => $this->getDisposalCO2dev(),
			"dataSourceUrl" => $this->getDataSourceUrl(),
			"date" => date('d.m.Y', $this->getDate()),
			"ean" => $this->getEan(),
			"upc" => $this->getUpc(),
			"emissionUnit" => $this->getEmissionUnit(),
			"extraData" => $this->getExtraData(),
			"source" => $this->getSource(),
			"versionInfo" => $this->getVersionInfo(),
			"externalId" => $this->getExternalId()
		];
	}
 }