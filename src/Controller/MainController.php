<?php

 namespace App\Controller;
 
 use Symfony\Component\HttpFoundation\Response;
 use App\Entity\Post;
 use App\Repository\PostRepository;
 use Doctrine\ORM\EntityManagerInterface;
 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\HttpFoundation\JsonResponse;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\Routing\Annotation\Route;

 class MainController extends AbstractController
 {
  /**
  * @Route("/")
  */
 public function indexAction() {
	 
	return new Response(file_get_contents(dirname(__DIR__, 2)."/frontend/react.html")); 
   //return new JsonResponse(array('message' => 'indexAction'));

   }
   
   /**
  * @Route("/testing", methods={"GET"})
  */
 public function testingAction() {

   return new JsonResponse(array('message' => 'testing!!!'));

   }

 }