<?php

 namespace App\Controller;
 
 use Symfony\Component\HttpFoundation\Response;
 use App\Entity\Factor;
 use App\Repository\PostRepository;
 use Doctrine\ORM\EntityManagerInterface;
 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\HttpFoundation\JsonResponse;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\Routing\Annotation\Route;

 /**
  * Class FactorController
  * @package App\Controller
  * @Route("/api", name="factor_api")
  */
 class FactorController extends AbstractController
 {
   /**
   * @param FactorRepository $factorRepository
   * @return JsonResponse
   * @Route("/index", name="apiIndex", methods={"GET"})
   */
  public function apiIndex(FactorRepository $postRepository){
	  
	 //return new Response(file_get_contents('../src/controller/home.html'));
	 //return new JsonResponse(array('message' => 'home'));
	 
	 return new JsonResponse(array('message' => 'No route created!'));
  }
  
   /**
   * @param FactorRepository $factorRepository
   * @return JsonResponse
   * @Route("/login", name="login", methods={"GET"})
   */
  public function login(FactorRepository $postRepository){
	  
	 //return new Response(file_get_contents('../src/controller/login.html'));
	 //return new JsonResponse(array('message' => 'apiIndex'));
	 
	 return new Response(file_get_contents(dirname(__FILE__)."/login.html"));
   
  }
  
  /**
   * @param FactorRepository $factorRepository
   * @return JsonResponse
   * @Route("/factors", name="factors", methods={"GET"})
   */
  public function getFactors(FactorRepository $factorRepository){
   $data = $postRepository->findAll();
   return $this->response($data);
  }

  /**
   * @param Request $request
   * @param EntityManagerInterface $entityManager
   * @param FactorRepository $factorRepository
   * @return JsonResponse
   * @throws \Exception
   * @Route("/factors", name="factors_add", methods={"POST"})
   */
  public function addFactor(Request $request, EntityManagerInterface $entityManager, FactorRepository $factorRepository){

   try{
    $request = $this->transformJsonBody($request);

    if (!$request || !$request->get('name') || !$request->request->get('description')){
     throw new \Exception();
    }

    $factor = new Factor();
    $factor->setName($request->get('name'));
    $factor->setDescription($request->get('description'));
    $entityManager->persist($factor);
    $entityManager->flush();

    $data = [
     'status' => 200,
     'success' => "Factor added successfully",
    ];
    return $this->response($data);

   }catch (\Exception $e){
    $data = [
     'status' => 422,
     'errors' => "Data no valid",
    ];
    return $this->response($data, 422);
   }

  }


  /**
   * @param FactorRepository $factorRepository
   * @param $id
   * @return JsonResponse
   * @Route("/factors/{id}", name="factors_get", methods={"GET"})
   */
  public function getFactor(FactorRepository $factorRepository, $id){
   $factor = $factorRepository->find($id);

   if (!$factor){
    $data = [
     'status' => 404,
     'errors' => "Factor not found",
    ];
    return $this->response($data, 404);
   }
   return $this->response($factor);
  }

  /**
   * @param Request $request
   * @param EntityManagerInterface $entityManager
   * @param FactorRepository $factorRepository
   * @param $id
   * @return JsonResponse
   * @Route("/factors/{id}", name="factors_put", methods={"PUT"})
   */
  public function updateFactor(Request $request, EntityManagerInterface $entityManager, FactorRepository $factorRepository, $id){

   try{
    $factor = $factorRepository->find($id);

    if (!$factor){
     $data = [
      'status' => 404,
      'errors' => "Factor not found",
     ];
     return $this->response($data, 404);
    }

    $request = $this->transformJsonBody($request);

    if (!$request || !$request->get('name') || !$request->request->get('description')){
     throw new \Exception();
    }

    $factor->setName($request->get('name'));
    $factor->setDescription($request->get('description'));
    $entityManager->flush();

    $data = [
     'status' => 200,
     'errors' => "Factor updated successfully",
    ];
    return $this->response($data);

   }catch (\Exception $e){
    $data = [
     'status' => 422,
     'errors' => "Data no valid",
    ];
    return $this->response($data, 422);
   }

  }


  /**
   * @param FactorRepository $factorRepository
   * @param $id
   * @return JsonResponse
   * @Route("/factors/{id}", name="factors_delete", methods={"DELETE"})
   */
  public function deleteFactor(EntityManagerInterface $entityManager, FactorRepository $factorRepository, $id){
   $factor = $factorRepository->find($id);

   if (!$factor){
    $data = [
     'status' => 404,
     'errors' => "Factor not found",
    ];
    return $this->response($data, 404);
   }

   $entityManager->remove($factor);
   $entityManager->flush();
   $data = [
    'status' => 200,
    'errors' => "Factor deleted successfully",
   ];
   return $this->response($data);
  }

  /**
   * Returns a JSON response
   *
   * @param array $data
   * @param $status
   * @param array $headers
   * @return JsonResponse
   */
  public function response($data, $status = 200, $headers = [])
  {
   return new JsonResponse($data, $status, $headers);
  }

  protected function transformJsonBody(\Symfony\Component\HttpFoundation\Request $request)
  {
   $data = json_decode($request->getContent(), true);

   if ($data === null) {
    return $request;
   }

   $request->request->replace($data);

   return $request;
  }

 }