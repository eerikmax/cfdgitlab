<?php

 namespace App\Controller;
 
 use Symfony\Component\HttpFoundation\Response;
 use App\Entity\Factor;
 use App\Repository\FactorRepository;
 use Doctrine\ORM\EntityManagerInterface;
 use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
 use Symfony\Component\HttpFoundation\JsonResponse;
 use Symfony\Component\HttpFoundation\Request;
 use Symfony\Component\Routing\Annotation\Route;

 /**
  * Class ApitestController
  * @package App\Controller
  * @Route("/factorapi", name="factorapi")
  */
 class FactorApiController extends AbstractController
 {
   /**
   * @param FactorRepository $factorRepository
   * @return JsonResponse
   * @Route("/index", name="apiIndex", methods={"GET"})
   */
  public function apiIndex(FactorRepository $factorRepository){
	  
	 //return new Response(file_get_contents('../src/controller/home.html'));
	 //return new JsonResponse(array('message' => 'home'));
	 
	 return new JsonResponse(array('message' => 'No route created!'));
  }
  
   /**
   * @param PostRepository $postRepository
   * @return JsonResponse
   * @Route("/searchfactor/{searchParam}", name="searchfactors", methods={"GET"})
   */
  public function searchFactors(Request $request, FactorRepository $factorRepository, $searchParam){
	//return new JsonResponse(array('word' => $searchFrom, 'searchParam' => $searchParam));
	
	$searchFrom = $request->query->get('searchFrom');
	$useRange = $request->query->get('useRange');
	if ($useRange == 'true') {
		$useRange = true;
		$rangeStart = $request->query->get('rangeStart');
		$rangeEnd = $request->query->get('rangeEnd');
	} else {
		$useRange = false;
	}
	
	$data = $factorRepository->findByBrandOrModel($searchParam, $searchFrom);

	if ($useRange) {
		//$rangeStart = 1292670089; //2010
		//$rangeEnd = 1450436489; //2015
		
		$newData = [];
		foreach ($data as $factor => $product) {
			if ($data[$factor]->getDate() >= $rangeStart && $data[$factor]->getDate() <= $rangeEnd) {
				$newData[] = $data[$factor];
			}
		}
		$data = $newData;
	}
	
	return $this->response($data);
  }
    
   /**
   * @param PostRepository $postRepository
   * @return JsonResponse
   * @Route("/search/{param}", name="search", methods={"GET"})
   */
  public function search(FactorRepository $factorRepository, $param){
	$data = $factorRepository->findByExampleField($param);	
	return $this->response($data);
	//return new JsonResponse(array('message' => $param));
  }
  
  /**
   * @param PostRepository $postRepository
   * @return JsonResponse
   * @Route("/factors", name="factors", methods={"GET"})
   */
  public function getFactors(Request $request, FactorRepository $factorRepository){
	//$data = $factorRepository->findAll();
	//return $this->response($data);
	
	$useRange = $request->query->get('useRange');
	if ($useRange == 'true') {
		$useRange = true;
		$rangeStart = $request->query->get('rangeStart');
		$rangeEnd = $request->query->get('rangeEnd');
	} else {
		$useRange = false;
	}

	$data = $factorRepository->findBy(
	   array(), // $where 
	   array('date' => 'DESC'), // $orderBy
	   100, // $limit
	   0 // $offset offsetia käytetään limitin kanssa, eli LIMIT 5 OFFSET 0 ottaa itemit 1..5, LIMIT 5 OFFSET 5 ottaa 5..10
	 );
	 
	/*if ($useRange) {
		$newData = [];
		foreach ($data as $factor => $product) {
			if ($data[$factor]->getDate() >= $rangeStart && $data[$factor]->getDate() <= $rangeEnd) {
				$newData[] = $data[$factor];
			}
		}
		$data = $newData;
	}*/
	 
	return $this->response($data);
  }

  /**
   * @param Request $request
   * @param EntityManagerInterface $entityManager
   * @param FactorRepository $factorRepository
   * @return JsonResponse
   * @throws \Exception
   * @Route("/factors", name="factors_add", methods={"POST"})
   */
  public function addFactor(Request $request, EntityManagerInterface $entityManager, FactorRepository $factorRepository){

   try{
    $request = $this->transformJsonBody($request);

    if (!$request || !$request->get('name') || !$request->request->get('description')){
     throw new \Exception();
    }

    $factor = new Factor();
    $factor->setName($request->get('name'));
    $factor->setDescription($request->get('description'));
    $entityManager->persist($factor);
    $entityManager->flush();

    $data = [
     'status' => 200,
     'success' => "Factor added successfully",
    ];
    return $this->response($data);

   }catch (\Exception $e){
    $data = [
     'status' => 422,
     'errors' => "Data no valid",
    ];
    return $this->response($data, 422);
   }

  }


  /**
   * @param FactorRepository $factorRepository
   * @param $id
   * @return JsonResponse
   * @Route("/factors/{id}", name="factors_get", methods={"GET"})
   */
  public function getPost(FactorRepository $factorRepository, $id){
   $factor = $factorRepository->find($id);

   if (!$factor){
    $data = [
     'status' => 404,
     'errors' => "Factor not found",
    ];
    return $this->response($data, 404);
   }
   return $this->response($factor);
  }

  /**
   * @param Request $request
   * @param EntityManagerInterface $entityManager
   * @param PostRepository $postRepository
   * @param $id
   * @return JsonResponse
   * @Route("/factors/{id}", name="factors_put", methods={"PUT"})
   */
  public function updateFactor(Request $request, EntityManagerInterface $entityManager, FactorRepository $factorRepository, $id){

   try{
    $factor = $factorRepository->find($id);

    if (!$factor){
     $data = [
      'status' => 404,
      'errors' => "Factor not found",
     ];
     return $this->response($data, 404);
    }

    $request = $this->transformJsonBody($request);

    if (!$request || !$request->get('name') || !$request->request->get('description')){
     throw new \Exception();
    }

    $factor->setName($request->get('name'));
    $factor->setDescription($request->get('description'));
	$entityManager->persist($factor);
    $entityManager->flush();

    $data = [
     'status' => 200,
     'errors' => "Factor updated successfully",
    ];
    return $this->response($data);

   }catch (\Exception $e){
    $data = [
     'status' => 422,
     'errors' => "Data no valid",
    ];
    return $this->response($data, 422);
   }

  }


  /**
   * @param FactorRepository $factorRepository
   * @param $id
   * @return JsonResponse
   * @Route("/factors/{id}", name="factors_delete", methods={"DELETE"})
   */
  public function deleteFactor(EntityManagerInterface $entityManager, FactorRepository $factorRepository, $id){
   $factor = $factorRepository->find($id);

   if (!$factor){
    $data = [
     'status' => 404,
     'errors' => "Factor not found",
    ];
    return $this->response($data, 404);
   }

   $entityManager->remove($factor);
   $entityManager->flush();
   $data = [
    'status' => 200,
    'errors' => "Factor deleted successfully",
   ];
   return $this->response($data);
  }

  /**
   * Returns a JSON response
   *
   * @param array $data
   * @param $status
   * @param array $headers
   * @return JsonResponse
   */
  public function response($data, $status = 200, $headers = [])
  {
   return new JsonResponse($data, $status, $headers);
  }

  protected function transformJsonBody(\Symfony\Component\HttpFoundation\Request $request)
  {
   $data = json_decode($request->getContent(), true);

   if ($data === null) {
    return $request;
   }

   $request->request->replace($data);

   return $request;
  }

 }