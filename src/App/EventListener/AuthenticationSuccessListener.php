<?php
# src/App/EventListener/AuthenticationSuccessListener.php
namespace App\App\EventListener;

use Lexik\Bundle\JWTAuthenticationBundle\Event\AuthenticationSuccessEvent;

class AuthenticationSuccessListener
{
    public function onAuthenticationSuccessResponse(AuthenticationSuccessEvent $event)
    {
		$user = $event->getUser();
		
        $event->setData([
            //'code' => $event->getResponse()->getStatusCode(),
			'username' => $user->getUsername(),
			'subscription' => $user->getSubscription(),
            'token' => $event->getData()['token'],
        ]);
    }
}